# doctests

It's really awkward to replicate, will dockerize.

Basically:

1. Set up the backend locally running with `docker-compose`. Make sure the branch has `api/fixtures/doctest.json`.
2. Install Racket and Make
3. run `raco pkg install request`
4. Run `make test DJANGO=path/to/zprava-api-service`

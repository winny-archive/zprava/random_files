# The server runs the the [zprava-api-service][zprava-api-service].

Note: To experiment and develop on the mobile client, consider running the server locally;
this way it is easier to debug issues, and can do whatever one pleases without disrupting the server configuration.

The MariaDB, Django application server, and the Caddy web-server all run within Docker.
In particular, the stack is managed by docker-compose.

There are three services to be aware of:

| Service name | What it does                                  | Where it is defined                               |
| ------------ | --------------------------------------------- | ------------------------------------------------- |
| `caddy`      | The web server, with automatic HTTPS support. | `/srv/caddy/docker-compose.yml`                   |
| `django`     | The [zprava-api-service][zprava-api-service]. | `/srv/zprava-api-service/docker-compose.prod.yml` |
| `mariadb`    | The MariaDB the `django` service uses.        | `/srv/zprava-api-service/docker-compose.prod.yml` |

Note: Everything is managed under user `deploy`, however both `/srv/dc.sh` and `/srv/deploy.sh` will switch to the correct user using `sudo`.
(Feel free to run these scripts as `ubuntu`.)

[zprava-api-service]: https://gitlab.com/zprava/zprava-api-service/

## View the logs of the services

Use `/srv/dc.sh logs` or `/srv/dc.sh logs service-name` (such as `django` or `caddy`).

## View HTTP/HTTPS access logs as seen by the web server (`caddy`)

View the log files in `/srv/caddy/logs/`, e.g. `less /srv/caddy/logs/zprava.philsauvey.com.log`.

Note: There is a seperate log for HTTP traffic to `zprava.philsauvey.com` at
`/srv/caddy/logs/http_zprava.philsauvey.com`.
The other logs only include HTTP traffic to their respected domains.

## Restart a service

Use `/srv/dc.sh restart` or `/srv/dc.sh restart service-name` (such as `django` or `caddy`).

## Obtain a python shell in the Django application

Use `/srv/dc.sh exec django python manage.py shell`.
One can also use this pattern to run other `manage.py` commands.

## Connect to the database

Using MySQL Workbench: [configure a connection that connects over SSH][mysql-workbench-ssh].

Using `mysql` or MySQL Workbench that doesn't support SSH:

```
ssh -L3306:127.0.0.1:3306 ubuntu@philsauvey.com
```

This forwards `127.0.0.1:3306` to the server's `127.0.0.1:3306` over SSH.

(Then connect to `127.0.0.1` on port `3306` in a different terminal.
Note: If one tries to connect to `localhost` with `mysql` command line client,
it will try to connect to `/var/run/mysqld/mysqld.sock` UNIX socket, which is not whan one wants, so use `127.0.0.1`.)

[mysql-workbench-ssh]: https://wavemotiondigital.com/tutorials/connecting-to-databases-via-ssh-with-mysql-workbench/

## How deployment works

Deployment from the `master` branch is automated.
Once unit tests pass, GitLab will install a purpose-generated ssh private key from the GitLab CI/CD stored variables.
Finally GitLab runs `ssh deploy@philsauvey.com /srv/deploy.sh`.
`/srv/deploy.sh` does three things:

1. It runs `git pull` to update the git repository,
2. It rebuilds the `django` docker image,
3. It restarts the `django` service with the new docker image.

Because of the security implications, this ssh key-pair is configured to only allow one command on the server and nothing else.
See `/home/deploy/.ssh/authorized_keys` for how that works.

## Specific configuration for production

- Django `DEBUG` is disabled.
- A unique secret key used to generate authentication tokens is used.
- `ALLOWED_SITES` includes `*` since Django is not publicly accessible - instead only accessible via Caddy.

## Annotated tree of the `/srv` directory

See also: [important-server-files](../important-server-files/).
Some items have been removed from the listing.

```
# tree -L 2 /srv
/srv
├── caddy                        # caddy webserver files
│   ├── Caddyfile                # main configuration file
│   ├── certs                    # Storage for HTTPS certifications obtained via LetsEncrypt
│   ├── docker-compose.yml       # Service definition for the caddy web server
│   └── logs                     # HTTP/HTTPS access logs
├── db                           # Database directory that holds databases in sub-directories
│   └── zprava-api-service       # MariaDB's /var/lib/mysql directory
├── dc.sh                        # Wrapper script to manage the stack. Always use this.
├── deploy.sh                    # Deploy script.
├── www                          # Static web directory for non-application sites
│   └── philsauvey.com           # web root for philsauvey.com
└── zprava-api-service           # zprava-api-service git repository
    ├── docker-compose.prod.yml  # Service definition for django + MariaDB suitable for production (contains secrets)
    ├── docker-compose.yml       # Service definition for development use.
    ├── docker-entrypoint.sh     # The script that runs in the django image when you start it.
    └── Dockerfile               # The Dockerfile used to build the django image.
```

#!/bin/sh
set -eux
# Check if we are deploy, otherwise run this command as deploy.
if [ "$(whoami)" != deploy ]; then
    exec sudo -u deploy -i /srv/deploy.sh "$@"  # FIXME change this line when file is moved.
fi
cd /srv/zprava-api-service
git pull
cd -

/srv/dc.sh stop -t 30 django
/srv/dc.sh up --build --no-deps -d django

# API
+ Username and ID should always be together
+ Make server disambiguate between adding a user by (1) ID, (2) Username, (3) Email

# Push notifications
+ GCM / FCM
+ jazzband django-push-notifications
+ Same mechanism for outside and inside app?

# Android
+ TODO: Add link to recycler view
+ Very great PoC by Nick
